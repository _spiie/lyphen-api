const mongoose = require('mongoose')
const Schema = mongoose.Schema

const User = mongoose.models.User || mongoose.model('User', new Schema({
  username: String, // Le nom de l'utilisateur.
  email: String, // L'email du compte (..)
  password: String, // Le mot de pass du compte (CHIFFRE)
  status: { type: String, default: 'Open' }, // Open, Closed, Private, Locked, Banned
  role: { type: String, default: 'User' } // Owner, Admin, Mod, User
}, { timestamps: true }))

module.exports = { User }
