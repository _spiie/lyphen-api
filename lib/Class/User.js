const { Database } = require('./Database')

class User {
  constructor (user) {
    this.user = user
  }

  // ACTIONS
  /// / Status actions
  async changeStatus (status = 'Open', reason = '') {
    const statusList = ['Open', 'Closed', 'Locked', 'Banned']
    if (!statusList.includes(status)) return 'Status unvalable'

    const db = new Database()
    db.updateUser(this.user, [{
      col: 'status',
      val: status
    }])
    db.disconnect()
  }

  /// /Roles Actions
  static async changeRole (roleName = 'User') {
  }

  /// / User Profil Actions
  static async changeUsername (newUsername) {
  }

  static async changePassword (newPassword) {
  }

  static async changeEmail (newEmail) {
  }
}

module.exports = { User }
