const mysql = require('mysql')
const { User } = require('./User')

class Database {
  constructor () {
    this.connection = mysql.createConnection({
      host: 'localhost',
      user: 'root',
      password: '123456',
      database: 'lyphen-db'
    })
  }

  disconnect () {
    this.connection.end()
  }

  getUsers () {
    return new Promise((resolve, reject) => {
      this.connection.query('SELECT * FROM users', (error, results, fields) => {
        if (error) throw error
        const users = []
        for (let i = 0; i < results.length; i++) {
          const user = new User(results[i])
          users.push(user)
        }
        resolve(results)
      })
    })
  }

  getUserById (id) {
    return new Promise((resolve, reject) => {
      this.connection.query(`SELECT * FROM users where id= '${id}'`, (error, results, fields) => {
        if (error) throw error
        resolve(new User(results[0]))
      })
    })
  }

  // FAIRE DES SOUS FONCTIONS EX : CHANGE.ID
  updateUser (user, changement) {
    changement = [
      {
        col: 'status',
        val: 'Open'
      }
    ]
    return new Promise((resolve, reject) => {
      const changements = ''

      this.connection.query(`UPDATE users SET id='${user.id}' where id='${user.id}'`, (error, results, fields) => {
        if (error) throw error
        console.log(results)
      })
    })
  }
}
module.exports = { Database }
