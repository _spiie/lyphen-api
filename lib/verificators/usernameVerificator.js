const { Database } = require('../Class/Database')

const usernameVerificator = async (username) => {
  username.trim()

  if (username.length < 4) return false
  const db = new Database()
  const users = await db.getUsers()
  let usernameFree = true
  for (let i = 0; i < users.length; i++) {
    const user = users[i]
    if (username === user.username) {
      console.log(user.username)
      usernameFree = false
      break
    }
  }
  return usernameFree
}

module.exports = { usernameVerificator }
