const bycript = require('bcrypt')

const { Database } = require('../../lib/Class/Database')

const emailValidator = async (email) => {
  email = email.toLowerCase()

  // eslint-disable-next-line
 const validRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (!validRegex.test(email)) {
    return false
  }

  const db = new Database()
  const users = await db.getUsers()
  let emailFree = true
  for (let i = 0; i < users.length; i++) {
    const user = users[i]

    if (await bycript.compare(email, user.email) === true) {
      emailFree = false
      break
    }
  }
  return emailFree
}

module.exports = { emailValidator }
