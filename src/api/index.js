const express = require('express');
const fs = require('fs');
const path = require('path');

const router = express.Router();

router.get('/', (req, res) => {
  res.json({
    message: 'API - 👋🌎🌍🌏',
  });
});

function readDirectory(dirPath) {
  fs.readdir(dirPath, (err, files) => {
    if (err) {
      console.log('Error reading directory: ', err);
      return;
    }
    if (firstTime) {
      for (let i = 0; i < files.length; i++) {
        if (files[i] === 'index.js') {
          files.splice(i, 1);
        }
      }

      firstTime = false;
    }
    files.forEach((file) => {
      const filePath = path.join(dirPath, file);

      fs.stat(filePath, (err, stats) => {
        if (err) {
          console.log('Error reading file statistics: ', err);
          return;
        }

        if (stats.isFile()) {
          let path = filePath.split('\\');
          path = `/${path.slice(2, path.length - 1).join('/')}`;
          try {
            const file = require(`../../${filePath.split('\\').join('/')}`);
            router.use(path, file);
          } catch (error) {

          }
        } else if (stats.isDirectory()) {
          readDirectory(filePath);
        }
      });
    });
  });
}
let firstTime = true;
readDirectory('./src/api');

module.exports = router;
