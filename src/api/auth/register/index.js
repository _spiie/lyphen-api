const express = require('express');

const router = express.Router();

router.get('/', async (req, res) => {
  // LIBS IMPORTS
  const bycript = require('bcrypt');
  const { PrismaClient } = require('@prisma/client');
  const prisma = new PrismaClient();

  // VERIFICATORS IMPORTS
  const { passwordValidator } = require('../../../../lib/verificators/passwordVerificator');
  const { usernameVerificator } = require('../../../../lib/verificators/usernameVerificator');
  const { emailValidator } = require('../../../../lib/verificators/emailVerificator');

  const { body } = req;
  const { email } = body;
  const { password } = body;
  const { username } = body;
  if (!email || !password || !username) {
    res.status(400);
    res.json({
      message: 'Il manque des informations.',
      email: !!email,
      password: !!password,
      username: !!username,
    });
  } else {
    const usernameValidity = await usernameVerificator(username);
    const passwordValidity = passwordValidator(password);
    const emailValidity = await emailValidator(email);
    if (!passwordValidity || !emailValidity) {
      res.status(400);
      res.json({
        message: 'Toutyes les informations ne sont pas valides.',
        email: emailValidity,
        password: passwordValidity,
        username: usernameValidity,
      });
    } else {
      await prisma.users.create({
        data: {
          email,
          password: await bycript.hash(password, 256),
          status: 'Open',
          role: 'User',
          username,
        },
      });
      res.status(200);
      res.json({
        message: 'Utilisateur enregistré !',
      });
    }
  }
  res.end();
});

module.exports = router;
