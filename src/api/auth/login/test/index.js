const express = require('express');

const router = express.Router();

router.get('/', async (req, res) => {
  // LIBS IMPORTS
  const bycript = require('bcrypt');
  const { PrismaClient } = require('@prisma/client');
  const prisma = new PrismaClient();
  const email = "mathieu@ss.fr"
  const password = "password"
  const user = await prisma.users.findUnique({
    where: {
      id: 2,
    }
  });
  if (!bycript.compare(password, user.password)){
    return res.status(401).json({ message: 'Email or password is incorrect.' });
  }
  req.session.userId = user.id;
  res.json({
    message: 'User Logged',
    user: user,
  });

  res.end();
});

module.exports = router;
