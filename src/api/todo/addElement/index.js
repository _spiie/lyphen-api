const express = require('express');
const middlewares = require('../../../middlewares');

const router = express.Router();

router.get('/', middlewares.isAuth, (req, res) => {
  // FAUT FAIRE L'AUTENTIFICATION ET ENLEVER CE PUTAIN D'ID EN PARAMETRES
  const { PrismaClient } = require('@prisma/client');
  const prisma = new PrismaClient();
  const { body } = req;
  const { newItem } = body;
  const { id } = body;

  if (!newItem || !id) {
    res.status(401);
    res.json({
      message: 'Missing parameters',
    });
    res.end();
  } else {
    const user = prisma.users.find();
    prisma.users.update({
      where: { id: user.id },
      data: changements,
    });
  }
});

module.exports = router;
