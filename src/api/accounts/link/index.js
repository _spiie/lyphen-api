const express = require('express');

const router = express.Router();

router.get('/', async (req, res) => {
  const { PrismaClient } = require('@prisma/client');
  const prisma = new PrismaClient();
  const bycript = require('bcrypt');

  const { body } = req;
  const { type } = body;
  const id = body.discordId;
  const { email } = body;
  const { password } = body;

  if (!type || !id || !email || !password) {
    res.json({
      message: 'Il manque des informations',
      type: !!type,
      id: !!id,
      email: !!email,
      password: !!password,
    });

    res.status(400);
  }

  const typeValidity = ['Discord'].includes(type);

  if (!typeValidity) {
    res.json({
      message: "Le type n'est pas valide",
      typeValidity,
    });
  }

  const users = await prisma.users.findMany();
  let valid = false;
  for (let i = 0; i < users.length; i++) {
    const user = users[i];
    if (bycript.compare(email, user.email) && bycript.compare(password, user.password)) {
      valid = true;
      let changements;
      if (type === 'Discord') {
        changements = {
          discordId: id,
        };
      }
      await prisma.users.update({
        where: { id: user.id },
        data: changements,
      });

      res.json({
        message: 'Lien effectué',
      });
      res.status(200);
      break;
    }
    if (!valid) {
      res.json({
        message: 'Pass / Email Invalid',
      });
      res.json(400);
    }
  }
  res.end();
});

module.exports = router;
