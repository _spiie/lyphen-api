/*
AJOUTER A LA DB DES LINKS (LIENS VERS D'AUTRES COMPTES) -> LINK DISCORD (id)

POUR LINK DISCORD IL FAUT CE CONNECTER A SON COMPTE LYPHEN (CONNECT) IF TRUE : AJOUTER ID A LINK DISCORD

VERIFIER QUE L'UTILISATEUR EST LIE A UN COMPTE AVANT DE FAIRE UN TRAITEMENT

SI CO ALORS FAIRE TRAITEMENT.

DEMANDER ID DE L'UTILISATEUR A CHAQUE DEBUT DE TRAITEMENTS
*/

const express = require('express');

const router = express.Router();

router.get('/', async (req, res) => {
  const { PrismaClient } = require('@prisma/client');
  const prisma = new PrismaClient();
  const { body } = req;
  const { id } = body;
  const { role } = body;
  const { reason } = body;
  const roleValidity = !!['User', 'Mod', 'Admin', 'Owner'].includes(role);
  const idValidity = !!await prisma.users.findFirst({
    where: {
      id,
    },
  });

  if (!roleValidity || !idValidity) {
    res.json({
      message: 'There is some invalid informations',
      role: roleValidity,
      idValidity,
    });
    res.status(400);
  } else {
    const userUpdate = await prisma.users.update({
      where: {
        id,
      },
      data: {
        role,
      },
    });
    console.log(userUpdate);
    res.json({
      message: 'User role Updated',
      user: userUpdate,
    });
    res.status(200);
  }

  res.end();
});

module.exports = router;
