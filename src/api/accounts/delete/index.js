const express = require('express');

const router = express.Router();

router.get('/', async (req, res) => {
  const { PrismaClient } = require('@prisma/client');
  const prisma = new PrismaClient();
  const { body } = req;
  const { id } = body;
  const { reason } = body;

  if (!id) {
    res.json({
      message: 'Missing Informations',
    });
    res.status(400);
  } else {
    const deletedUder = await prisma.users.delete({
      where: {
        id,
      },
    });

    res.json({
      message: `User ${id} delleted.`,
      user: deletedUder,
    });

    res.status(200);
  }

  res.end();
});

module.exports = router;
